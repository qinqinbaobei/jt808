package com.ltmonitor.jt808.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.ltmonitor.entity.Terminal;
import com.ltmonitor.entity.TerminalCommand;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.jt808.protocol.JT_0001;
import com.ltmonitor.jt808.protocol.JT_0100;
import com.ltmonitor.jt808.protocol.JT_0102;
import com.ltmonitor.jt808.protocol.JT_0201;
import com.ltmonitor.jt808.protocol.JT_0500;
import com.ltmonitor.jt808.protocol.JT_0801;
import com.ltmonitor.jt808.protocol.JT_8001;
import com.ltmonitor.jt808.protocol.JT_8100;
import com.ltmonitor.jt808.protocol.JT_8800;
import com.ltmonitor.jt808.protocol.T808Message;
import com.ltmonitor.jt808.protocol.T808MessageHeader;
import com.ltmonitor.jt808.service.IAckService;
import com.ltmonitor.jt808.service.ICommandService;
import com.ltmonitor.jt808.service.IMessageSender;
import com.ltmonitor.jt808.service.ITransferGpsService;
import com.ltmonitor.jt809.entity.VehicleRegisterInfo;
import com.ltmonitor.service.IVehicleService;
import com.ltmonitor.service.JT808Constants;

//应答服务，对于终端上传的命令，进行应答
public class AckService implements IAckService {

	private static Logger logger = Logger.getLogger(AckService.class);
	private ConcurrentLinkedQueue<T808Message> dataQueue = new ConcurrentLinkedQueue();

	private ICommandService commandService;
	private Thread processRealDataThread;
	private IMessageSender messageSender;

	private IVehicleService vehicleService;

	private String authencateNo = "1234567890A"; // 鉴权码

	private ConcurrentHashMap vehicleRegisterMap = new ConcurrentHashMap();
	private ConcurrentHashMap terminalRegisterMap = new ConcurrentHashMap();

	// 数据转发服务,主要用于809转发
	private ITransferGpsService transferGpsService;

	private boolean checkRegister;

	private boolean transferTo809Enabled;

	public AckService() {
		processRealDataThread = new Thread(new Runnable() {
			public void run() {
				ProcessRealDataThreadFunc();
			}
		});
		processRealDataThread.start();
	}

	public void beginAck(T808Message tm) {
		dataQueue.add(tm);
	}

	private void ProcessRealDataThreadFunc() {

		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(20);
		int times = 0;
		while (true) {
			try {
				if(times > 0 && times % 10 == 0 && dataQueue.size() > 0)
				{
					logger.error("等待应答队列数:"+dataQueue.size());
				}
				
				T808Message tm = dataQueue.poll();
				final List<T808Message> msgList = new ArrayList<T808Message>();
				while (tm != null) {
					msgList.add(tm);
					if (msgList.size() > 100)
						break;
					tm = dataQueue.poll();
				}
				if (msgList.size() > 0) {
					fixedThreadPool.execute(new Runnable() {
						@Override
						public void run() {
							SendGeneralAck(msgList);
						}
					});
				}

			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
			
			times++;
			try {
				Thread.sleep(200L);
			} catch (InterruptedException e1) {
			}
		}
	}

	private void SendGeneralAck(List<T808Message> msgList) {
		for (T808Message tm : msgList) {
			SendGeneralAck(tm);
		}
	}

	/**
	 * 对终端发送上来的消息进行通用应答
	 * 
	 * @param msgFromTerminal
	 *            终端消息
	 */
	private void SendGeneralAck(T808Message msgFromTerminal) {
		int msgType = msgFromTerminal.getMessageType();
		if (msgType == 0)
			return;

		String simNo = msgFromTerminal.getSimNo();

		// 终端注册
		if (msgType == 0x0100) {
			// 对终端注册需要由平台进行应答
			JT_0100 registerData = (JT_0100) msgFromTerminal
					.getMessageContents();
			if (registerData == null) {
				return;
			}
			msgFromTerminal.setPlateNo(registerData.getLicenseNo());
			int result = 0;// 注册成功
			if (checkRegister) {
				// 0：成功；1：车辆已被注册；2：数据库中无该车辆；3：终端已被注册；4：数据库中无该终端
				VehicleData vd = vehicleService
						.getVehicleByPlateNo(msgFromTerminal.getPlateNo());

				if (vd != null) {
					if (vehicleRegisterMap.containsKey(vd.getPlateNo())) {
						if (terminalRegisterMap.containsKey(registerData
								.getTerminalId()))
							result = 1;// 车辆已被注册
					}

					vehicleRegisterMap.put(vd.getPlateNo(), 1);

					if (result == 0) {
						Terminal term = vehicleService
								.getTerminalByTermNo(registerData
										.getTerminalId());
						if (term == null) {
							result = 4;// 数据库中无该终端
						} else {
							if (terminalRegisterMap.containsKey(term
									.getTermNo())) {
								result = 3;// 终端已被注册
							}
							terminalRegisterMap.put(term.getTermNo(), 1);
						}
					}

				} else
					result = 2;// 数据库中无该车辆；
			}

			JT_8100 echoData = new JT_8100();
			echoData.setRegisterResponseMessageSerialNo(msgFromTerminal
					.getHeader().getMessageSerialNo());
			echoData.setRegisterResponseResult((byte) result);
			echoData.setRegisterNo(authencateNo);

			T808Message ts = new T808Message();
			ts.setMessageContents(echoData);
			ts.setHeader(new T808MessageHeader());
			ts.getHeader().setMessageType(0x8100);
			ts.getHeader().setSimId(simNo);
			// ts.Header.MessageSize = echoData.WriteToBytes().Length;
			ts.getHeader().setIsPackage(false);

			getMessageSender().Send808Message(ts);
			//转发给809服务器
			if (transferTo809Enabled) {
				VehicleData vd = vehicleService.getVehicleBySimNo(simNo);
				if (vd != null && vd.getTermId() > 0) {
					Terminal terminal = vehicleService.getTerminal(vd
							.getTermId());

					if (terminal != null) {

						VehicleRegisterInfo vi = new VehicleRegisterInfo();
						vi.setPlateNo(vd.getPlateNo());
						vi.setPlateColor(vd.getPlateColor());
						vi.setSimNo(vd.getSimNo());
						vi.setTerminalId(terminal.getTermNo()); // 终端编号
						vi.setTerminalModel(terminal.getTermType());
						vi.setTerminalVendorId(terminal.getMakeNo());// 厂商编号

						this.transferGpsService.transferRegisterInfo(vi); // 终端鉴权后，上传注册信息
					}
				}
			}

		} else if (msgType == 0x0001) {
			// 如果是终端通用应答，就更新数据库的指令状态为已应答
			JT_0001 answerData = (JT_0001) msgFromTerminal.getMessageContents();
			short platformSn = answerData.getResponseMessageSerialNo();
			String cmdStatus = answerData.getResponseResult() == 0 ? TerminalCommand.STATUS_SUCCESS
					: TerminalCommand.STATUS_FAILED;
			if (answerData.getResponseResult() >= 2) {
				cmdStatus = TerminalCommand.STATUS_NOT_SUPPORT;
			}

			TerminalCommand tc = this.commandService.UpdateStatus(
					msgFromTerminal.getHeader().getSimId(), platformSn,
					cmdStatus);
			if (tc != null && TerminalCommand.FROM_GOV.equals(tc.getOwner())) {
				VehicleData vd = vehicleService
						.getVehicleBySimNo(tc.getSimNo());
				// 如果是上级平台下发的指令，则需要转发给上级平台
				int result = answerData.getResponseResult() == 0 ? 0 : 1;
				if (tc.getCmdType() == JT808Constants.CMD_DIAL_BACK) {
					// 单向监听应答
					this.transferGpsService.transferListenTermianlAck(
							vd.getPlateNo(), vd.getPlateColor(), (byte) result);
				} else if (tc.getCmdType() == JT808Constants.CMD_CONTROL_TERMINAL) {
					// 紧急接入应答
					this.transferGpsService.transferEmergencyAccessAck(
							vd.getPlateNo(), vd.getPlateColor(), (byte) result);
				} else if (tc.getCmdType() == JT808Constants.CMD_SEND_TEXT) {
					int msgId = Integer.parseInt(tc.getCmd());
					// 紧急接入应答
					this.transferGpsService.transferTextInfoAck(
							vd.getPlateNo(), vd.getPlateColor(), msgId,
							(byte) result);
				}
			}

		} else if (msgType == 0x0201) {
			// 终端对位置查询（点名）的应答
			JT_0201 answerData = (JT_0201) msgFromTerminal.getMessageContents();
			short platformSn = answerData.getResponseMessageSerialNo();
			String cmdStatus = TerminalCommand.STATUS_SUCCESS;
			commandService.UpdateStatus(msgFromTerminal.getHeader().getSimId(),
					platformSn, cmdStatus);
		} else if (msgType == 0x0500) {
			// 终端对车门控制的应答
			JT_0500 answerData = (JT_0500) msgFromTerminal.getMessageContents();
			short platformSn = answerData.getResponseMessageSerialNo();
			String cmdStatus = TerminalCommand.STATUS_SUCCESS;
			commandService.UpdateStatus(msgFromTerminal.getHeader().getSimId(),
					platformSn, cmdStatus);
		} else if (msgType == 0x0104) {
			// 查询终端参数应答
			JT_8001 echoData = new JT_8001();
			echoData.setResponseMessageSerialNo(msgFromTerminal.getHeader()
					.getMessageSerialNo());
			echoData.setResponseMessageId((short) msgFromTerminal.getHeader()
					.getMessageType());
			echoData.setResponseResult((byte) 0);

			T808Message ts = new T808Message();
			ts.setMessageContents(echoData);
			ts.setHeader(new T808MessageHeader());
			ts.getHeader().setMessageType(0x8001);
			ts.getHeader().setSimId(msgFromTerminal.getHeader().getSimId());
			// ts.Header.MessageSize = echoData.WriteToBytes().Length;
			ts.getHeader().setIsPackage(false);
			getMessageSender().Send808Message(ts);

		} else if (msgType == 0x0805) {
			// 多媒体数据上传命令应答
			logger.info(msgType);

		} else if (msgType == 0x0801) {
			// 多媒体上传，要在所有的包上传完毕后应答.
			if (msgFromTerminal.getHeader().getMessageTotalPacketsCount() == msgFromTerminal
					.getHeader().getMessagePacketNo()) {
				JT_0801 cmdData = (JT_0801) msgFromTerminal
						.getMessageContents();
				if (cmdData != null) {
					JT_8800 echoData = new JT_8800();
					echoData.setMultimediaDataId(cmdData.getMultimediaDataId());
					echoData.setRepassPacketsCount((byte) 0);
					echoData.setRepassPacketsNo(new ArrayList<Short>());

					T808Message ts = new T808Message();
					ts.setMessageContents(echoData);
					ts.setHeader(new T808MessageHeader());
					ts.getHeader().setMessageType(0x8800);
					ts.getHeader().setSimId(simNo);
					// ts.Header.MessageSize = echoData.WriteToBytes().Length;
					ts.getHeader().setIsPackage(false);
					getMessageSender().Send808Message(ts);
				}
			}
		} else {
			int ackResult = 0;// 通用应答，成功标志
			if (msgType == 0x0102) {
				// 终端鉴权，要向上级平台注册信息
				JT_0102 cmdData = (JT_0102) msgFromTerminal
						.getMessageContents();
				if (cmdData != null) {
					ackResult = this.authencateNo.equals(cmdData
							.getRegisterNo()) ? 0 : 1; // 鉴权成功或失败
					if (this.authencateNo.equals(cmdData.getRegisterNo())) {
						/**
						 * VehicleData vd = vehicleService
						 * .GetVehicleBySimNo(msgFromTerminal.getSimNo()); if
						 * (vd != null && vd.getTermId() > 0) { Terminal
						 * terminal = vehicleService.getTerminal(vd
						 * .getTermId());
						 * 
						 * VehicleRegisterInfo vi = new VehicleRegisterInfo();
						 * vi.setPlateNo(vd.getPlateNo());
						 * vi.setPlateColor(vd.getPlateColor());
						 * vi.setSimNo(vd.getSimNo());
						 * vi.setTerminalId(terminal.getTermNo()); // 终端编号
						 * vi.setTerminalModel(terminal.getTermType());
						 * vi.setTerminalVendorId(terminal.getMakeNo());// 厂商编号
						 * 
						 * this.transferGpsService.transferRegisterInfo(vi); //
						 * 终端鉴权后，上传注册信息 }
						 */
					}
				}
			}
			// 对于终端发送的其他命令，平台一律进行通用应答
			JT_8001 echoData = new JT_8001();
			echoData.setResponseMessageSerialNo(msgFromTerminal.getHeader()
					.getMessageSerialNo());
			echoData.setResponseMessageId((short) msgType);
			echoData.setResponseResult((byte) ackResult); // 应答成功

			T808Message ts = new T808Message();
			ts.setMessageContents(echoData);
			ts.setHeader(new T808MessageHeader());
			ts.getHeader().setMessageType(0x8001);
			ts.getHeader().setSimId(simNo);
			ts.getHeader().setIsPackage(false);
			getMessageSender().Send808Message(ts);
		}

	}

	public void setMessageSender(IMessageSender messageSender) {
		this.messageSender = messageSender;
	}

	public IMessageSender getMessageSender() {
		return messageSender;
	}

	public ICommandService getCommandService() {
		return commandService;
	}

	public void setCommandService(ICommandService commandService) {
		this.commandService = commandService;
	}

	public ITransferGpsService getTransferGpsService() {
		return transferGpsService;
	}

	public void setTransferGpsService(ITransferGpsService transferGpsService) {
		this.transferGpsService = transferGpsService;
	}

	public IVehicleService getVehicleService() {
		return vehicleService;
	}

	public void setVehicleService(IVehicleService vehicleService) {
		this.vehicleService = vehicleService;
	}

	public String getAuthencateNo() {
		return authencateNo;
	}

	public void setAuthencateNo(String authencateNo) {
		this.authencateNo = authencateNo;
	}

	public boolean isCheckRegister() {
		return checkRegister;
	}

	public void setCheckRegister(boolean checkRegister) {
		this.checkRegister = checkRegister;
	}

	public boolean isTransferTo809Enabled() {
		return transferTo809Enabled;
	}

	public void setTransferTo809Enabled(boolean transferTo809Enabled) {
		this.transferTo809Enabled = transferTo809Enabled;
	}

}
