package com.ltmonitor.service;

import java.util.List;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.BasicData;
import com.ltmonitor.entity.PlatformState;

public interface IBaseService extends IBaseDao{

	PlatformState getPlatformState();

	List<BasicData> getInformationByType(String inforType);

}
