package com.ltmonitor.service.impl;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.JT809Command;
import com.ltmonitor.entity.TerminalCommand;
import com.ltmonitor.service.ITerminalService;

public class TerminalService implements ITerminalService {
	private IBaseDao baseDao;
	
	/**
	 * 下发指令
	 */
	@Override
	public void SendCommand(TerminalCommand tc )
	{
		baseDao.save(tc);
	}
	
	/**
	 * 下发指令
	 */
	public void SendPlatformCommand(JT809Command tc )
	{
		baseDao.save(tc);
	}
	
	public IBaseDao getBaseDao() {
		return baseDao;
	}
	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

}
