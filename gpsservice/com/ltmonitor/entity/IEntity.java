﻿package com.ltmonitor.entity;

public interface IEntity
{
	//实体ID
	int getEntityId();
	void setEntityId(int value);

	//所属租户ID
	int getTenantId();
	void setTenantId(int value);

}